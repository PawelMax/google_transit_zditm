package pl.SDA.core.reader;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import pl.SDA.core.model.Agency;
import pl.SDA.core.parser.AgencyParser;
import pl.SDA.core.parser.FeedParser;
import pl.SDA.core.repository.AgencyRepository;
import pl.SDA.core.repository.FeedRepository;

import java.util.List;

public class AgencyFeedReaderTest {

    private FeedReader<Agency> agencyFeedReader;
    private FeedRepository<Agency> repository;
    private FeedParser<Agency> agencyParser;

    @Before
    public void setUp() throws Exception {
        agencyParser = new AgencyParser();
        repository = new AgencyRepository();
        agencyFeedReader = new AgencyFeedReader(agencyParser, repository); //kolejność ma znaczenie
    }

    @Test
    public void testReaderAgency() {
        //given

        //when
        agencyFeedReader.read();
        List<Agency> list = repository.getAll();

        //then
        assertTrue(!list.isEmpty());
        assertEquals(1, list.size());
    }


}