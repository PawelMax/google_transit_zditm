package pl.SDA.core.reader;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import pl.SDA.core.model.Route;
import pl.SDA.core.parser.FeedParser;
import pl.SDA.core.parser.RouteParser;
import pl.SDA.core.repository.FeedRepository;
import pl.SDA.core.repository.RouteRepository;

import java.util.List;

public class RouteFeedReaderTest {

    private FeedReader<Route> routeFeedReader;
    private FeedParser<Route> routeFeedParser;
    private FeedRepository<Route> routeFeedRepository;

    @Before
    public void setUp() throws Exception {
        routeFeedParser = new RouteParser();
        routeFeedRepository = new RouteRepository();
        routeFeedReader = new RouteFeedReader(routeFeedParser, routeFeedRepository);
    }

    @Test
    public void testReaderRoute() {
        //when
        routeFeedReader.read();
        List<Route> list = routeFeedRepository.getAll();

        //then
        assertTrue(!list.isEmpty());
        assertEquals(100, list.size());
    }

}