package pl.SDA.core.repository;

import org.junit.Before;
import org.junit.Test;
import pl.SDA.core.model.Trip;
import pl.SDA.core.parser.FeedParser;
import pl.SDA.core.parser.TripParser;
import pl.SDA.core.reader.FeedReader;
import pl.SDA.core.reader.TripFeedReader;
import static org.junit.Assert.*;


import java.util.List;

public class TripRepositoryTest {

    private FeedReader<Trip> tripFeedReader;
    private FeedRepository<Trip> repository;
    private FeedParser<Trip> tripFeedParser;

    @Before
    public void setUp() throws Exception {
        tripFeedParser = new TripParser();
        repository = new TripRepository();
        tripFeedReader = new TripFeedReader(tripFeedParser, repository);
    }

    @Test
    public void testReaderTrips(){
        //given

        //when
        tripFeedReader.read();
        List<Trip> list = repository.getAll();

        //then
        assertTrue(!list.isEmpty());
        assertEquals(19844, list.size());
    }

}