package pl.SDA.core.parser;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import pl.SDA.core.model.Agency;

public class AgencyParserTest {

    private static final String TEST_LINE1 = "ZDiTM Szczecin,http://www.zditm.szczecin.pl,Europe/Warsaw,pl,91 480 04 03";
    private static final String TEST_LINE2 = ",,,,";
    private FeedParser<Agency> agencyParser;
    private Agency agency1, agency2;

    @Before
    public void setUp() throws Exception {
        agencyParser = new AgencyParser();
        agency1 = new Agency("ZDiTM Szczecin", "http://www.zditm.szczecin.pl", "Europe/Warsaw", "pl", "91 480 04 03");
        agency2 = new Agency("", "", "", "", "");
    }

    @Test
    public void testParseAgency1() {
        //given
        Agency testAgency;

        //when
        testAgency = agencyParser.parseTo(TEST_LINE1);

        //then
        assertEquals(testAgency.getAgencyName(), agency1.getAgencyName());
        assertEquals(testAgency.getAgencyUrl(), agency1.getAgencyUrl());
        assertEquals(testAgency.getAgencyTimezone(), agency1.getAgencyTimezone());
        assertEquals(testAgency.getAgencyLang(), agency1.getAgencyLang());
        assertEquals(testAgency.getAgencyPhone(), agency1.getAgencyPhone());
    }

    @Test
    public void testParseAgency2() {
        //given
        Agency testAgency;

        //when
        testAgency = agencyParser.parseTo(TEST_LINE2);

        //then
        assertEquals(testAgency.getAgencyName(), agency2.getAgencyName());
        assertEquals(testAgency.getAgencyUrl(), agency2.getAgencyUrl());
        assertEquals(testAgency.getAgencyTimezone(), agency2.getAgencyTimezone());
        assertEquals(testAgency.getAgencyLang(), agency2.getAgencyLang());
        assertEquals(testAgency.getAgencyPhone(), agency2.getAgencyPhone());
    }


}