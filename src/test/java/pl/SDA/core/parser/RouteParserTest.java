package pl.SDA.core.parser;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import pl.SDA.core.model.Route;
import pl.SDA.core.model.RouteType;

public class RouteParserTest {

    private static final String TEST_LINE1 = "\"2\",\"2\",\"Dworzec Niebuszewo - Turkusowa\",,0,\"http://www.zditm.szczecin.pl/rozklady/index.html\",,";
    private static final String TEST_LINE2 = "\",\"\",\"\",,,,,";
    private FeedParser<Route> routeFeedParser;
    private Route route1, route2;

    @Before
    public void setUp() throws Exception {
        routeFeedParser = new RouteParser();
        route1 = new Route("2", "2", "Dworzec Niebuszewo - Turkusowa", "", RouteType.TRAM, "http://www.zditm.szczecin.pl/rozklady/index.html", "", "");
        route2 = new Route("", "", "", "", RouteType.UNKNOWN, "", "", "");

    }

    @Test
    public void testParseRoute1() {
        //given
        Route testRoute;

        //when
        testRoute = routeFeedParser.parseTo(TEST_LINE1);

        //then
        compareResults(testRoute, route1);
    }

    @Test
    public void testParseRoute2() {
        //given
        Route testRoute;

        //when
        testRoute = routeFeedParser.parseTo(TEST_LINE2);

        //then
        compareResults(testRoute, route2);
    }

    private void compareResults(Route expected, Route route2) {
        assertEquals(expected.getRouteId(), route2.getRouteId());
        assertEquals(expected.getRouteShortName(), route2.getRouteShortName());
        assertEquals(expected.getRouteLongName(), route2.getRouteLongName());
        assertEquals(expected.getRouteDesc(), route2.getRouteDesc());
        assertEquals(expected.getRouteUrl(), route2.getRouteUrl());
        assertEquals(expected.getRouteColor(), route2.getRouteColor());
        assertEquals(expected.getRouteTextColor(), route2.getRouteTextColor());
    }


}