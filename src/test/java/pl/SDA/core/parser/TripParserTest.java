package pl.SDA.core.parser;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import pl.SDA.core.model.Trip;

public class TripParserTest {

    private static final String TEST_LINE = "\"A\",\"POW\",\"A-7549-POW-1-1-0\",\"Plac Rodła\",\"0\",\"A/1\",665,0,1";
    private FeedParser<Trip> tripFeedParser;
    private Trip trip;

    @Before
    public void setUp() throws Exception {
        tripFeedParser = new TripParser();
        trip = new Trip("A", "POW", "A-7549-POW-1-1-0", "Plac Rodła", "0", "A/1", "665", false, true);
    }

    @Test
    public void testParceTrips(){
        //given
        Trip testTrip;

        //when
        testTrip = tripFeedParser.parseTo(TEST_LINE);

        //then
        assertEquals(testTrip.getRouteId(), trip.getRouteId());
        assertEquals(testTrip.getServiceId(), trip.getServiceId());
        assertEquals(testTrip.getTripId(), trip.getTripId());
        assertEquals(testTrip.getTripHeadsign(), trip.getTripHeadsign());
        assertEquals(testTrip.getDirectionId(), trip.getDirectionId());
        assertEquals(testTrip.getBlockId(), trip.getBlockId());
        assertEquals(testTrip.getShapeId(), trip.getShapeId());
        assertEquals(testTrip.isWheelchairAccessible(), trip.isWheelchairAccessible());
        assertEquals(testTrip.isLowFloor(), trip.isLowFloor());


    }
}