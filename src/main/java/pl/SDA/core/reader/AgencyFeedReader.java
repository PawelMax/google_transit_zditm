package pl.SDA.core.reader;

import pl.SDA.core.model.Agency;
import pl.SDA.core.parser.FeedParser;
import pl.SDA.core.repository.FeedRepository;

public class AgencyFeedReader extends BaseFeedReader<Agency> {

    public AgencyFeedReader(FeedParser<Agency> parse, FeedRepository<Agency> repository){
        super(parse, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/agency.txt";
    }
}
