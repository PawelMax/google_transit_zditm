package pl.SDA.core.reader;

import pl.SDA.core.model.Trip;
import pl.SDA.core.parser.FeedParser;
import pl.SDA.core.repository.FeedRepository;

public class TripFeedReader extends BaseFeedReader<Trip>{

    public TripFeedReader(FeedParser<Trip> parser, FeedRepository<Trip> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/trips.txt";
    }
}
