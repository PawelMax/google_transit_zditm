package pl.SDA.core.reader;

import pl.SDA.core.model.Route;
import pl.SDA.core.parser.FeedParser;
import pl.SDA.core.repository.FeedRepository;

public class RouteFeedReader extends BaseFeedReader<Route> {

    public RouteFeedReader(FeedParser<Route> parser, FeedRepository<Route> repository){
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/routes.txt";
    }
}
