package pl.SDA.core.reader;

import pl.SDA.core.model.FeedModel;

public interface FeedReader<T extends FeedModel> {
    void read();

    String getFileDir();

}
