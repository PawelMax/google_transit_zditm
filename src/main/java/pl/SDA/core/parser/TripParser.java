package pl.SDA.core.parser;

import pl.SDA.core.model.Agency;
import pl.SDA.core.model.Trip;

public class TripParser extends BaseFeedParser<Trip> {

    private static final byte TRIP_ROUTEID_COLUMN = 0;
    private static final byte TRIP_SERVICEID_COLUMN = 1;
    private static final byte TRIP_ID_COLUMN = 2;
    private static final byte TRIP_HEADSIGN_COLUMN = 3;
    private static final byte TRIP_DIRECTIONID_COLUMN = 4;
    private static final byte TRIP_BLOCKID_COLUMN = 5;
    private static final byte TRIP_SHAPEID_COLUMN = 6;
    private static final byte TRIP_WHEELCHAIR_COLUMN = 7;
    private static final byte TRIP_LOWFLOOR_COLUMN = 8;

    public Trip parseTo(String feedInfo) {
        super.parseTo(feedInfo);
        Trip trip = new Trip();
        trip.setRouteId(splitFeedInfo[TRIP_ROUTEID_COLUMN]);
        trip.setServiceId(splitFeedInfo[TRIP_SERVICEID_COLUMN]);
        trip.setTripId(splitFeedInfo[TRIP_ID_COLUMN]);
        trip.setTripHeadsign(splitFeedInfo[TRIP_HEADSIGN_COLUMN]);
        trip.setDirectionId(splitFeedInfo[TRIP_DIRECTIONID_COLUMN]);
        trip.setBlockId(splitFeedInfo[TRIP_BLOCKID_COLUMN]);
        trip.setShapeId(splitFeedInfo[TRIP_SHAPEID_COLUMN]);
        trip.setWheelchairAccessible(Integer.parseInt(splitFeedInfo[TRIP_WHEELCHAIR_COLUMN]) == 1);
        //inna opcja zapisu
        /*int wheelchair_accessible = Integer.parseInt(splitFeedInfo[TRIP_WHEELCHAIR_COLUMN]);
        if (wheelchair_accessible == 1) {
            trip.setWheelchairAccessible(true);
        } else {
            trip.setWheelchairAccessible(false);
        }*/
        trip.setLowFloor(Integer.parseInt(splitFeedInfo[TRIP_LOWFLOOR_COLUMN]) == 1);
        return trip;
    }

    @Override
    public String parseFrom(Trip feed) {
        return null;
    }

}
