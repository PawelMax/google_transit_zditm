package pl.SDA.core.parser;

import pl.SDA.core.model.FeedModel;

public abstract class BaseFeedParser <T extends FeedModel> implements FeedParser<T>{

    protected String[] splitFeedInfo;
    public BaseFeedParser(){
        super();
    }

    public T parseTo(String line) {
        line = line.replaceAll("\"", "");
        splitFeedInfo = line.split(FeedParser.DATA_DELIMITER, -1);
        return null;
    }
}
