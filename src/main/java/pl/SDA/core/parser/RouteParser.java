package pl.SDA.core.parser;

import pl.SDA.core.model.Route;
import pl.SDA.core.model.RouteType;

public class RouteParser extends BaseFeedParser<Route> {

    private static final byte ROUTE_ID_COLUMN = 0;
    private static final byte ROUTE_SHORTNAME_COLUMN = 1;
    private static final byte ROUTE_LONGNAME_COLUMN = 2;
    private static final byte ROUTE_DESC_COLUMN = 3;
    private static final byte ROUTE_TYPE_COLUMN = 4;
    private static final byte ROUTE_URL_COLUMN = 5;
    private static final byte ROUTE_COLOR_COLUMN = 6;
    private static final byte ROUTE_TEXTCOLOR_COLUMN = 7;

    public Route parseTo(String feedInfo) {
        super.parseTo(feedInfo); //split line to the table
        Route route = new Route();
        route.setRouteId(splitFeedInfo[ROUTE_ID_COLUMN]);
        route.setRouteShortName(splitFeedInfo[ROUTE_SHORTNAME_COLUMN]);
        route.setRouteLongName(splitFeedInfo[ROUTE_LONGNAME_COLUMN]);
        route.setRouteDesc(splitFeedInfo[ROUTE_DESC_COLUMN]);
        String typeNumber = splitFeedInfo[ROUTE_TYPE_COLUMN];
        if (!typeNumber.isEmpty()){
            route.setRouteType(RouteType.getType(Integer.parseInt(splitFeedInfo[ROUTE_TYPE_COLUMN])));
        }else {
            route.setRouteType(RouteType.UNKNOWN);
        }
        route.setRouteUrl(splitFeedInfo[ROUTE_URL_COLUMN]);
        route.setRouteColor(splitFeedInfo[ROUTE_COLOR_COLUMN]);
        route.setRouteTextColor(splitFeedInfo[ROUTE_TEXTCOLOR_COLUMN]);
        return route;
    }

    public String parseFrom(Route feed) {
        return null;
    }
}
