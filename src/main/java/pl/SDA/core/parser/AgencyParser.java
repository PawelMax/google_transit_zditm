package pl.SDA.core.parser;

import pl.SDA.core.model.Agency;

public class AgencyParser extends BaseFeedParser<Agency> {

    private static final byte AGENCY_NAME_COLUMN = 0;
    private static final byte AGENCY_URL_COLUMN = 1;
    private static final byte AGENCY_TIMEZONE_COLUMN = 2;
    private static final byte AGENCY_LANG_COLUMN = 3;
    private static final byte AGENCY_PHONE_COLUMN = 4;

    public Agency parseTo(String feedInfo) {
        super.parseTo(feedInfo); // split feedInfo to table
        Agency agency = new Agency();
        agency.setAgencyName(splitFeedInfo[AGENCY_NAME_COLUMN]);
        agency.setAgencyUrl(splitFeedInfo[AGENCY_URL_COLUMN]);
        agency.setAgencyTimezone(splitFeedInfo[AGENCY_TIMEZONE_COLUMN]);
        agency.setAgencyLang(splitFeedInfo[AGENCY_LANG_COLUMN]);
        agency.setAgencyPhone(splitFeedInfo[AGENCY_PHONE_COLUMN]);
        return agency;
    }

    public String parseFrom(Agency feed) {
        return null;
    }
}
