package pl.SDA.core.model;

import pl.SDA.utils.TextUtils;


public class Agency extends FeedModel {
    private String stopId;
    private String stopCode;
    private String stopName;
    private String stopDesc;
    private double stopLat;
    private double stopLon;
    private String stopUrl;


    public Agency() {
        super(TextUtils.EMPTY_STRING);
    }


}
