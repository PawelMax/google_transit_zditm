package pl.SDA.core.repository;

import pl.SDA.core.model.Agency;

import java.util.ArrayList;
import java.util.List;

public class AgencyRepository implements FeedRepository<Agency> {

    private final List<Agency> agencyList = new ArrayList<>();

 //   @Override // override w nowszym IntelliJeju
    public void insert(Agency feed) {
        agencyList.add(feed);
    }

    public void delete(Agency feed) {
        agencyList.remove(feed);
    }

    public void delete(String id) {

    }

    public void update(Agency feed) {

    }

    public String add(Agency feed) {
        return null;
    }

    public List<Agency> getAll() {
        return agencyList;
    }
}
