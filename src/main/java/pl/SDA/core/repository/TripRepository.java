package pl.SDA.core.repository;

import pl.SDA.core.model.FeedModel;
import pl.SDA.core.model.Trip;

import java.util.ArrayList;
import java.util.List;

public class TripRepository implements FeedRepository<Trip> {

    private final List<Trip> tripsList = new ArrayList<>();


    public void insert(Trip feed) {
        tripsList.add(feed);
    }

    public void delete(Trip feed) {
        tripsList.remove(feed);
    }

    @Override
    public void delete(String id) {
    }

    @Override
    public void update(Trip feed) {
    }

    @Override
    public String add(Trip feed) {
        return null;
    }

    @Override
    public List getAll() {
        return tripsList;
    }
}
