package pl.SDA.core.repository;

import pl.SDA.core.model.Route;

import java.util.ArrayList;
import java.util.List;

public class RouteRepository implements FeedRepository<Route> {

    private final List<Route> routeList = new ArrayList<>();

    //   @Override // override w nowszym IntelliJeju
    public void insert(Route feed) {
        routeList.add(feed);
    }

    public void delete(Route feed) {
        routeList.remove(feed);
    }

    public void delete(String id) {

    }

    public void update(Route feed) {

    }

    public String add(Route feed) {
        return null;
    }

    public List<Route> getAll() {
        return routeList;
    }
}
