package pl.SDA.module;

import pl.SDA.core.model.Agency;
import pl.SDA.core.repository.AgencyRepository;
import pl.SDA.core.repository.FeedRepository;

public class RepositoryModule {

    private static FeedRepository<Agency> agencyFeedRepository;

    public static FeedRepository<Agency> provideAgencyRepository(){
        if (agencyFeedRepository == null) {
            agencyFeedRepository = new AgencyRepository();
        }
        return agencyFeedRepository;
    }
}
