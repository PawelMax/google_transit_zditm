package pl.SDA.module;

import pl.SDA.core.model.Agency;
import pl.SDA.core.model.Route;
import pl.SDA.core.parser.FeedParser;
import pl.SDA.core.reader.AgencyFeedReader;
import pl.SDA.core.reader.FeedReader;
import pl.SDA.core.reader.RouteFeedReader;
import pl.SDA.core.reader.TripFeedReader;
import pl.SDA.core.repository.FeedRepository;
import pl.SDA.core.model.Trip;

public class FeedReaderModule {

    private static FeedReader<Agency> agencyFileReader;
    private static FeedReader<Route> routeFileReader;
    private static FeedReader<Trip> tripFilerReader;


    public static FeedReader<Agency> provideAgencyFileReader(FeedParser<Agency> parser, FeedRepository<Agency> repository) {
        if (agencyFileReader == null) {
            agencyFileReader = new AgencyFeedReader(parser, repository);
        }
        return agencyFileReader;
    }

    //Optionally
//    public static FeedReader<Agency> provideAgencyFileReader() {
//        if (agencyFileReader == null) {
//            agencyFileReader = new AgencyFeedReader(ParserModule.provideAgencyParser(), RepositoryModule.)
//        }
//    }
    public static  FeedReader<Route> provideRouteFileReader(FeedParser<Route> parser, FeedRepository<Route> repository){
        if(routeFileReader == null){
            routeFileReader = new RouteFeedReader(parser, repository);
        }
        return routeFileReader;
    }



    public static FeedReader<Trip> provideTripFileReader(FeedParser<Trip> parser, FeedRepository<Trip> repository){
        if(tripFilerReader == null){
            tripFilerReader = new TripFeedReader(parser, repository);
        }
        return tripFilerReader;
    }
}
