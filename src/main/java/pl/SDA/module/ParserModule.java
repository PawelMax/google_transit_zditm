package pl.SDA.module;

import pl.SDA.core.model.Agency;
import pl.SDA.core.parser.AgencyParser;
import pl.SDA.core.parser.FeedParser;

public class ParserModule {

    private static AgencyParser agencyParser;

    public static FeedParser<Agency> provideAgencyParser() {
        if (agencyParser == null) {
            agencyParser = new AgencyParser();
        }
        return agencyParser;
    }
}
